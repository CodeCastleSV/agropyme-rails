Rails.application.routes.draw do
  devise_for :users
  devise_scope :user do
    root to: "devise/sessions#new"
  end

  resources :dashboard
  resources :roles
  resources :users
  resources :farms
  resources :maps do 
    collection do 
      get 'kml_viewer/:id' => 'maps#kml_viewer'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
