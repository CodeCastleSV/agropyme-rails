class CreateFarms < ActiveRecord::Migration[5.2]
  def change
    create_table :farms do |t|
      t.string :name, null: false
      t.string :address, null: true
      t.text :polygon, null: false
      t.timestamps
    end
  end
end
