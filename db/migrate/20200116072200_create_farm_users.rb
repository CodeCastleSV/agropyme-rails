class CreateFarmUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :farm_users do |t|
      t.references :user
      t.references :farm
      t.timestamps
    end
  end
end
