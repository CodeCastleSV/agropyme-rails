class AddFileToMap < ActiveRecord::Migration[5.2]
  def change
    add_column :maps, :file, :string
  end
end
