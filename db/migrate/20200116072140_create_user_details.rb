class CreateUserDetails < ActiveRecord::Migration[5.2]
  def change
    create_table :user_details do |t|
      t.string :name, null: false
      t.string :last_name, null: false
      t.text :address, null: true
      t.string :phone_number, null: false
      t.string :customer_number, null: true
      t.string :dui, null: true
      t.string :nit, null: true
      t.text :description, null: true
      t.references :user
      t.timestamps
    end
  end
end
