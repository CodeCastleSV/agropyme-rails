# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Role.create(name: "Administrador")
User.create(email: "yventura@codecastle.com.sv", password: "123123123", password_confirmation: "123123123", role: Role.first)
User.create(email: "wgonzalez@codecastle.com.sv", password: "123123123", password_confirmation: "123123123", role: Role.first)
UserDetail.create(name: "Yareli", last_name: "Ventura", address: "1era calle poniente y 63 avenida norta local c6", phone_number: "7777-9999", customer_number: "110101", dui: "098765-4", nit: "0612-190393-101-3", description: "Propietario de Finca Castillo", user: User.first)
UserDetail.create(name: "Williams", last_name: "Gonzalez", address: "1era calle poniente y 63 avenida norta local c6", phone_number: "7777-9999", customer_number: "110101", dui: "098765-4", nit: "0612-190393-101-3", description: "Propietario de Finca Castillo", user: User.last)
