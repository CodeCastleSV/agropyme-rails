class FarmUser < ApplicationRecord
  belongs_to :farm, optional: true
  belongs_to :user, optional: true
end
