class Farm < ApplicationRecord
  mount_uploader :photo, FarmPhotoUploader
  has_many :users, through: :farm_users
  has_many :maps
  serialize :polygon
end
