class Map < ApplicationRecord
  mount_uploader :file, MapFileUploader
  belongs_to :farm, optional: true
end
