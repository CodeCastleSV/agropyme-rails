class RolesController < ApplicationController


  def index
    @roles = Role.all.order("id DESC")
  end

  def new
    @role = Role.new
  end

  def create
    role = Role.new(permit_params)
    redirect_to roles_path if role.save
  end

  def edit
    @role = Role.find(params[:id])
  end

  def update
    role = Role.find(params[:id])
    redirect_to roles_path if role.update_attributes(permit_params)
  end

  def permit_params
    params.require(:role).permit(:name)
  end

end