class MapsController < ApplicationController


  def index
    @maps = Map.all.order("id DESC")
  end

  def new
    @map = Map.new
  end

  def create
    map = Map.new(permit_params)
    redirect_to maps_path if map.save
  end

  def edit
    @map = Map.find(params[:id])
  end

  def update
    map = Map.find(params[:id])
    redirect_to maps_path if map.update_attributes(permit_params)
  end

  def kml_viewer
    @map = Map.find(params[:id])
  end

  def permit_params
    params.require(:map).permit(:name, :description, :farm_id, :file)
  end

end