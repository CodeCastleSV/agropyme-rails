class FarmsController < ApplicationController


  def index
    @farms = Farm.all.order("id DESC")
  end

  def new
    @farm = Farm.new
  end

  def create
    farm = Farm.new(name: permit_params[:name], address: permit_params[:address], photo: permit_params[:photo], polygon: permit_params[:polygon].to_json)
    if farm.save
      FarmUser.create(farm_id: farm.id, user_id: params[:farm][:user_id])
      redirect_to farms_path 
    end
  end

  def edit
    @farm = Farm.find(params[:id])
  end

  def update
    farm = Farm.find(params[:id])
    if farm.update_attributes(permit_params)
      uf = FarmUser.find_by(farm_id: farm.id)
      uf.update_attributes(user_id: params[:farm][:user_id])
      redirect_to farms_path 
    end
  end

  def permit_params
    params.require(:farm).permit(:name, :address, :photo, :polygon)
  end

end